Source: ams
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Dennis Braun <d_braun@kabelmail.de>,
 Free Ekanayaka <freee@debian.org>,
 Junichi Uekawa <dancer@debian.org>,
 Alessio Treglia <alessio@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ladspa-sdk,
 libasound2-dev,
 libclalsadrv-dev (>= 1.0.1-3),
 libfftw3-dev,
 libjack-dev,
 liblo-dev,
 libqt5opengl5-dev,
 qtbase5-dev,
 qttools5-dev-tools,
 sfftw-dev
Rules-Requires-Root: no
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/multimedia-team/ams.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/ams
Homepage: http://alsamodular.sourceforge.net/

Package: ams
Architecture: any
Depends:
 cmt,
 mcp-plugins,
 swh-plugins,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 amb-plugins,
 rev-plugins,
 vco-plugins,
 jackd
Description: Realtime modular synthesizer for ALSA
 AlsaModularSynth is a realtime modular synthesizer and effect processor.
 It features:
  * MIDI controlled modular software synthesis.
  * Realtime effect processing with capture from e.g. "Line In" or "Mic In".
  * Full control of all synthesis and effect parameters via MIDI.
  * Integrated LADSPA Browser with search capability.
  * JACK Support.
